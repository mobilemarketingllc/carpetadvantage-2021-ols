<?php do_action( 'fl_content_close' ); ?>

	</div><!-- .fl-page-content -->
	<?php

	do_action( 'fl_after_content' );

	if ( FLTheme::has_footer() ) :

		?>
	<footer class="fl-page-footer-wrap"<?php FLTheme::print_schema( ' itemscope="itemscope" itemtype="https://schema.org/WPFooter"' ); ?>  role="contentinfo">
		<?php

		do_action( 'fl_footer_wrap_open' );
		do_action( 'fl_before_footer_widgets' );

		FLTheme::footer_widgets();

		do_action( 'fl_after_footer_widgets' );
		do_action( 'fl_before_footer' );

		FLTheme::footer();

		do_action( 'fl_after_footer' );
		do_action( 'fl_footer_wrap_close' );

		?>
	</footer>
	<?php endif; ?>
	<?php do_action( 'fl_page_close' ); ?>
</div><!-- .fl-page -->
<?php

wp_footer();

do_action( 'fl_body_close' );

FLTheme::footer_code();

?>
</body>
<!-- phone insertion script begins -->
<!-- 
<script type='text/javascript' src='https://reports.hibu.com/analytics/js/ybDynamicPhoneInsertion.js'></script>
<script>
ybFindPhNums = ['12173374980', '12173374980'];
ybReplacePhNums = ['12172502466', '12172502467'];

document.addEventListener("YextPhoneChangeEvent", yextPhoneChangeEventHandler, false);

function yextPhoneChangeEventHandler(e) {
e.preventDefault();
ybFun_ReplaceText();
}

if (typeof dmAPI != 'undefined') {
dmAPI.runOnReady('dpni', function() {
setTimeout(ybFun_ReplaceText, 500);
});
dmAPI.subscribeEvent(dmAPI.EVENTS.SHOW_POPUP, function(data) {
setTimeout(ybFun_ReplaceText, 500);
console.log('dmAPI.EVENTS.SHOW_POPUP' + data);
});
} else {
window.onload = function() {
setTimeout(ybFun_ReplaceText, 500);
}
}

</script> -->
<!-- phone insertion script ends -->
<style>iframe[name="google_conversion_frame"] {height: 0 !important;width: 0 !important;line-height: 0 !important;font-size: 0 !important;margin-top: -13px;float: left;}</style><script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"52003226"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=52003226&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>


</html>
